package es.franam.filmaffinity.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import es.franam.filmaffinity.models.Genero;
import es.franam.filmaffinity.models.Pelicula;

public interface IPeliculaJPA extends JpaRepository<Pelicula, Long> {
	
	 public List<Pelicula> findByTituloContainingIgnoreCase(String titulo);
	 
	
	 public List<Pelicula> findByGenero(String genero);
	 
	 //public List<Pelicula> findByActorContainingIgnoreCase(String nombre);
	 
	 
}
