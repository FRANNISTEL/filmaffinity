package es.franam.filmaffinity.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.franam.filmaffinity.dao.IPeliculaJPA;
import es.franam.filmaffinity.models.Genero;
import es.franam.filmaffinity.models.Pelicula;

@Service
public class PeliculaServiceImpl implements IPeliculaService {

	@Autowired
	private IPeliculaJPA peliculaJPA;

	@Override
	@Transactional(readOnly = true)
	public List<Pelicula> findAll() {
		return peliculaJPA.findAll();
	}

	@Override
	@Transactional
	public Pelicula save(Pelicula pelicula) {
		return peliculaJPA.save(pelicula);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		peliculaJPA.deleteById(id);

	}

	@Override
	@Transactional(readOnly = true)
	public Pelicula findById(Long id) {
		return peliculaJPA.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Pelicula> findByTitulo(String titulo) {
		return peliculaJPA.findByTituloContainingIgnoreCase(titulo);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Pelicula> findByGenero(String genero) {
		return peliculaJPA.findByGenero(genero);
	}

}
