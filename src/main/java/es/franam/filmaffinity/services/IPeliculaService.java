package es.franam.filmaffinity.services;

import java.util.List;
import java.util.Optional;

import es.franam.filmaffinity.models.Genero;
import es.franam.filmaffinity.models.Pelicula;

public interface IPeliculaService {

	public List<Pelicula> findAll();
	
	public Pelicula save(Pelicula pelicula);
	
	public void delete(Long id);
	
	public Pelicula findById(Long id);
	
	public List<Pelicula> findByTitulo(String titulo);
	
	public List<Pelicula> findByGenero(String genero);
	
	//public List<Pelicula> findByActorContainingIgnoreCase(String nombre);
	
}
