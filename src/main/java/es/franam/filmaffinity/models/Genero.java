package es.franam.filmaffinity.models;


public enum Genero {
	ACCION, DRAMA, AVENTURA, COMEDIA, TERROR, MUSICAL, CIENCIA_FICCION, GUERRA, SUSPENSE, OESTE, INFANTILES;
}
