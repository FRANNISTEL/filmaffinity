package es.franam.filmaffinity.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.franam.filmaffinity.models.Genero;
import es.franam.filmaffinity.models.Pelicula;
import es.franam.filmaffinity.services.IPeliculaService;

@CrossOrigin(origins = { "https://localhost:4200" })
@RestController
@RequestMapping("/api")
public class PeliculaRestController {

	@Autowired
	private IPeliculaService peliculaService;

	@GetMapping("/peliculas")
	public List<Pelicula> listar() {
		return peliculaService.findAll();
	}

	@GetMapping("/peliculas/{id}")
	public Pelicula mostrarPorId(@PathVariable Long id) {
		return peliculaService.findById(id);
	}

	@PostMapping("/peliculas")
	@ResponseStatus(HttpStatus.CREATED)
	public Pelicula crear(@RequestBody Pelicula pelicula) {
		return peliculaService.save(pelicula);
	}

	@PutMapping("/peliculas/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Pelicula actualizar(@RequestBody Pelicula pelicula, @PathVariable Long id) {
		Pelicula peliculaActual = peliculaService.findById(id);
		peliculaActual.setTitulo(pelicula.getTitulo());
		peliculaActual.setAnyo(pelicula.getAnyo());
		peliculaActual.setDireccion(pelicula.getDireccion());
		peliculaActual.setDuracion(pelicula.getDuracion());
		peliculaActual.setGenero(pelicula.getGenero());
		peliculaActual.setPais(pelicula.getPais());
		peliculaActual.setSinopsis(pelicula.getSinopsis());

		return peliculaService.save(peliculaActual);

	}

	@DeleteMapping("/peliculas/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		peliculaService.delete(id);
	}
	
	@GetMapping("/peliculas/{titulo}")
	public List<Pelicula> mostrarPorTitulo(@PathVariable String titulo) {
		return peliculaService.findByTitulo(titulo);
	}
	
	@GetMapping("/peliculas/{genero}")
	public List<Pelicula> mostrarPorGenero(@PathVariable String genero) {
		return peliculaService.findByGenero(genero);
	}
	 
	
	
	//public List<Pelicula> findByActorContainingIgnoreCase(String nombre);

}
