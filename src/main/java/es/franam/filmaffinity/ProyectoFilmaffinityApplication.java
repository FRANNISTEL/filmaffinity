package es.franam.filmaffinity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoFilmaffinityApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoFilmaffinityApplication.class, args);
	}

}
